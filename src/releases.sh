#!/usr/bin/env bash


function extract_relevant_information_to_file() {
    # Extract relevant information to file
    #
    # The releases extracted is expected to be kept.
    # Arguments:
    #  $1: filename for json file fron gitlab containing releases
    #  $2: filename for output file
    #  Returns: json file with relevant information
    local file="${1:-./releases.json}"
    local output="${2:-./relevant_release_info.json}"
    jq '[
            .[] |
                select(
                    (.tag_name | test("^v"))
                    or (
                        (.tag_name | test("^rc"))
                        and (
                            .created_at | split("T")[0] |
                                . > (now-60*60*24*180|strflocaltime("%Y-%m-%d"))
                        )
                    )
                    or (
                        .created_at | split("T")[0] |
                            . > (now-60*60*24*7|strflocaltime("%Y-%m-%d"))
                    )
                ) |
                {
                    name: .name,
                    tag_name: .tag_name,
                    created_at: .created_at | split("T")[0],
                    assets_links_name: .assets.links[0].name,
                    assets_links_direct_url: .assets.links[0].direct_asset_url
                }
        ]' "${file}" > "${output}"
}


function _extract_values_for_key_from_relevant_info() {
    # Extract tag names for array
    #
    # Is meant to be used with mapfile like this:
    # local result=()
    # mapfile -t result < <(_extract_values_for_key_from_relevant_info "key_name")
    # Arguments:
    #  $1: key_name
    #  Returns: tag names
    local key_name="${1}"
    local file="${2:-./relevant_release_info.json}"
    jq -r ".[].${key_name}" "${file}"
}


function _create_json_array_from_bash_array() {
    # Create json array from bash array
    # Arguments:
    #  $1: bash array
    #  Returns: json array
    local array=("$@")
    jq -nc '$ARGS.positional' --args "${array[@]}"
}


function _remove_quotation_marks_from_array() {
    # Remove quotation marks from array
    #
    # Is meant to be used with mapfile and read like this to get a valid bash array:
    # mapfile -t array < <(_remove_quotation_marks_from_array "${array[@]}")
    # local tmp=()
    # read -a tmp <<< "${array[@]}"
    # Arguments:
    # $1: bash array
    # Returns: strings without quotation marks
    local array=("$@")
    local result=()
    for i in "${array[@]}"; do
        result+=("${i//\"/}")
    done
    echo "${result[@]}"
    }


function _remove_four_chars_from_end_array() {
    # Remove extension from array
    #
    # Is meant to be used with mapfile and read like this to get a valid bash array:
    # mapfile -t array < <(_remove_four_chars_from_end_array "${array[@]}")
    # local tmp=()
    # read -a tmp <<< "${array[@]}"
    # Arguments:
    # $1: bash array
    # Returns: strings with four characters removed from end
    local array=("$@")
    local result=()
    for i in "${array[@]}"; do
        result+=("${i:0:-4}")
    done
    echo "${result[@]}"
}


function extract_releases_to_remove() {
    # Extract releases to remove
    #
    # The releases extracted is expected to be removed.
    # Saved to file releases_to_remove.txt
    # Arguments:
    #  $1: filename for json file containing objects to be kept default ./relevant_release_info.json
    local file="${1:-./releases.json}"
    local keep_id=""
    local tmp=()
    local tmp2=()
    mapfile -t tmp < <(_extract_values_for_key_from_relevant_info "tag_name")
    mapfile -t tmp < <(_remove_quotation_marks_from_array "${tmp[@]}")
    read -a tmp <<< "${tmp[@]}"
    keep_id="$(_create_json_array_from_bash_array "${tmp[@]}")"
    tmp2=$(jq --argjson keep "${keep_id[@]}" '.[]
        | select(.tag_name as $in | $keep | index($in) | not)
        | .tag_name' "${file}")
    mapfile -t tmp2 < <(_remove_quotation_marks_from_array "${tmp2[@]}")
    echo "${tmp2[@]}" | tr " " "\n" > ./releases_to_remove.txt
}


function extract_packages_to_remove() {
    # Extract releases to remove
    #
    # The releases extracted is expected to be removed.
    # Dev packages are kept for 14 days.
    # packages with the same version as the kept releases are kept.
    # Saved to file packages_to_remove.txt
    # Arguments:
    #  $1: filename for json file containing objects to be kept default ./relevant_release_info.json
    local file="${1:-./packages.json}"
    local keep_id=""
    local tmp=()
    mapfile -t tmp < <(_extract_values_for_key_from_relevant_info "assets_links_name")
    mapfile -t tmp < <(_remove_four_chars_from_end_array "${tmp[@]}")
    read -a tmp <<< "${tmp[@]}"
    keep_id="$(_create_json_array_from_bash_array "${tmp[@]}")"
    jq --argjson keep "${keep_id[@]}" '.[]
        | select(
            (
                .created_at | split("T")[0] |
                    . > (now-60*60*24*14|strflocaltime("%Y-%m-%d")) | not
            )
            and (
                .version as $in | $keep | index($in) | not
            )
        )
        | .id' "${file}" > ./packages_to_remove.txt
}


# function remove_releases() {
#     # Remove releases
#     # Arguments:
#     #  $1: gitlab project id
#     #  $2: gitlab token
#     #  $3: gitlab url
#     #  $4: array of release ids
#     local project_id="${1}"
#     local token="${2}"
#     local url="${3}"
#     local releases=("$4")
#     for release in "${releases[@]}"; do
#         printf "%s\n" "Deleting release: ${release}"
#         curl -X DELETE --header "PRIVATE-TOKEN: ${token}" "${url}/api/v4/projects/${project_id}/releases/${release}"
#     done
# }
