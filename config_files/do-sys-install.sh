# Script to download install and setup scripts for do-system


_download_file () {
    local url="${1}"
    local dest="${2}"
    printf "%s\n" "### Downloading $url to $dest ###"
    curl "$url" -o "$dest"
}

install_do_sys_scripts_root () {
    printf "%s\n" "### Installing do-system scripts ###"
    local branch="${1}"
    local install_scripts=(
        "3-system-config.sh"
        "5-post-install.sh"
    )
    local install_dir="/root/install"
    local url="https://gitlab.com/olesendan/infrastructure/-/raw/${branch}/src/installation/arch-linux"

    mkdir -p "$install_dir"
    for script in "${install_scripts[@]}"; do
        printf "%s\n" "### Downloading $script ###"
        _download_file "$url/$script" "$install_dir/$script"
        chmod +x "$install_dir/$script"
    done
    printf "%s\n" "### Done installing do-system scripts ###"
    printf "%s\n" "### Run the following commands to configure do-system ###"
    printf "%s\n" "### ./root/install/3-system-config.sh ###"
}

main () {
    install_do_sys_scripts_root "main"
}

if [ "${BASH_SOURCE[0]}" -ef "$0" ]; then
    printf "%s\n" "### Script is being executed. ###"
    main
else
    printf "%s\n" "### Script is being sourced. ###"
fi
