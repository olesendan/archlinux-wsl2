export DO_SYS_ENV_FILE="/etc/profile.d/do-system.sh"
export DO_SYS_SCRIPT_SYSTEM_CONFIG=0
export DO_SYS_SCRIPT_POST_INSTALL=0
export DO_SYS_ROOTFS_DEVICE=""
export DO_SYS_SYSTEM_TYPE="wsl2"
export DO_SYS_HOSTNAME=""
export DO_SYS_CPU_VENDOR=""
export DO_SYS_GPU_VENDOR=""
