# archlinux-wsl2


## Summary

Repository for an opionated archlinux installation on wsl2.

The end result should be a setup where archlinux is integrated with a windows 11 machine for software development.


## Prerequisites

   * windows 11 enterprice
   * wsl2 installed
   * chocolately installed


## Getting started

Download the artifact from the main branch. Which is a bootstrapped archlinux installatation from a docker image.

on windows powershell
Install chocolately per their instructions.

after installing choco, use powershell to install powershell 7.x.x
```
choco install pwsh
```

In powershell 7.x.x
```
choco install alacritty
choco install brave
choco install git
```

Install other relevant programs on the windows side

   * sql server management studio
      sql-server-management-studio
   * Azure service bus explorer
      servicebusexplorer
   * Azure Storage Explorer
      microsoftazurestorageexplorer

## download archlinux-wsl2

goto gitlab.com olesendan/archlinux-wsl2 under releases download latest stable version


## Install Archlinux

Run pwsh as administrator (select option from right menu).

pwsh
```powershell
wsl --install --no-distribustion

wsl --set-default-version 2

wsl --import <distro-name> <path/to/dir/for/distro> <path/to/downloaded/tar/file>

wsl --set-default <distro-name>
```



download nerd fonts
jetbrainsmono

FiraCode mono

unpack to fonts folder in onedrive, if not present.




Download the following files from the repo, and place them in your home folder on windows

   * .wslconfig
   * alacritty.toml


when installed wsl, the system needs to be configured. There is provided scripts for system config.

on windows start alacritty

```powershell
wsl.exe -d <distro-name>
```

This should open archlinux

```shell
# cd ~/install
# chmod +x 3-system-config.sh 5-post-install.sh
# ./3-system-config.sh
# ./5-post-install.sh
```
alacritty.exe

