# script which contains tests for cleanup releases and packages

source ../src/releases.sh


function test_extract_relevant_information_to_file() {
    extract_relevant_information_to_file ./releases.json
    echo "Test extract_relevant_information_to_file"
    cat ./relevant_release_info.json
    echo "---------------------------------"
}
test_extract_relevant_information_to_file


function test_extract_values_for_key_from_relevant_info_tag_name() {
    echo "Test extract_values_for_key_from_relevant_info for tag_name"
    local result=()
    mapfile -t result < <(_extract_values_for_key_from_relevant_info "tag_name")
    echo "${result[@]}"
    for i in "${result[@]}"; do
        echo "----"
        echo "${i}"
    done
    echo "---------------------------------"
}
test_extract_values_for_key_from_relevant_info_tag_name


function test_extract_values_for_key_from_relevant_info_assets_links_name() {
    echo "Test extract_values_for_key_from_relevant_info for assets_links_name"
    local result=()
    mapfile -t result < <(_extract_values_for_key_from_relevant_info "assets_links_name")
    echo "${result[@]}"
    for i in "${result[@]}"; do
        echo "----"
        echo "${i}"
    done
    echo "---------------------------------"
}
test_extract_values_for_key_from_relevant_info_assets_links_name


function test_remove_four_chars_from_end_array() {
    echo "Test remove_four_chars_from_end_array"
    local array=("test1.tar" "test2.tar" "test3.tar")
    local result=()
    mapfile -t array < <(_remove_four_chars_from_end_array "${array[@]}")
    read -a result <<< "${array[@]}"
    echo "${result[@]}"
    for i in "${result[@]}"; do
        echo "----"
        echo "${i}"
    done
    echo "---------------------------------"
}
test_remove_four_chars_from_end_array


function test_remove_quotation_marks_from_array() {
    echo "Test remove_quotation_marks_from_array"
    local array=("test1" "test2" "test3")
    local result=()
    mapfile -t array < <(_remove_quotation_marks_from_array "${array[@]}")
    read -a result <<< "${array[@]}"
    echo "${result[@]}"
    for i in "${result[@]}"; do
        echo "----"
        echo "${i}"
    done
    echo "---------------------------------"
}
test_remove_quotation_marks_from_array


function test_create_json_array_from_bash_array() {
    echo "Test create_json_array_from_bash_array"
    local array=("test1" "test2" "test3")
    _create_json_array_from_bash_array "${array[@]}"
    echo "---------------------------------"
}
test_create_json_array_from_bash_array


function test_create_json_array_from_bash_array2() {
    echo "Test create_json_array_from_bash_array2"
    local array=("test1.tar" "test2.tar" "test3.tar")
    echo "array:"
    echo "${array[@]}"
    mapfile -t array < <(_remove_four_chars_from_end_array "${array[@]}")
    local keep=()
    read -a keep <<< "${array[@]}"
    echo "keep:"
    echo "${keep[@]}"
    _create_json_array_from_bash_array "${keep[@]}"
    echo "---------------------------------"
}
test_create_json_array_from_bash_array2


function test_create_json_array_from_bash_array3() {
    echo "Test create_json_array_from_bash_array3"
    local array=("test1" "test2" "test3")
    echo "array:"
    echo "${array[@]}"
    mapfile -t array < <(_remove_quotation_marks_from_array "${array[@]}")
    local keep=()
    read -a keep <<< "${array[@]}"
    echo "keep:"
    echo "${keep[@]}"
    _create_json_array_from_bash_array "${keep[@]}"
    echo "---------------------------------"
}
test_create_json_array_from_bash_array3


function test_extract_releases_to_remove() {
    echo "Test extract_releases_to_remove"
    extract_releases_to_remove
    local array=()
    mapfile -t array < ./releases_to_remove.txt
    for i in "${array[@]}"; do
        echo "----"
        echo "${i}"
    done
    echo "---------------------------------"
}
test_extract_releases_to_remove

function test_extract_packages_to_remove() {
    echo "Test extract_packages_to_remove"
    extract_packages_to_remove
    local array=()
    mapfile -t array < ./packages_to_remove.txt
    for i in "${array[@]}"; do
        echo "----"
        echo "${i}"
    done
    echo "---------------------------------"
}
test_extract_packages_to_remove
