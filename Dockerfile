# Version: 1.0
# Author: Dan Olesen
# This Dockerfile will create an opionated bootstrapped tar file based on the latest archlinux image

FROM olesendan/archlinux-docker:latest
ARG ARCHLINUX_VERSION
ENV ARCHLINUX_VERSION=${ARCHLINUX_VERSION}

USER root
RUN pacman-key --init \
   && pacman-key --populate \
   && pacman -Syu --noconfirm --needed

RUN mkdir ./config_files
COPY ./config_files/* ./config_files/

RUN curl "http://mirrors.dotsrc.org/archlinux/iso/${ARCHLINUX_VERSION}/archlinux-bootstrap-${ARCHLINUX_VERSION}-x86_64.tar.gz" -o archlinux-bootstrap-${ARCHLINUX_VERSION}-x86_64.tar.gz \
   && curl "http://mirrors.dotsrc.org/archlinux/iso/${ARCHLINUX_VERSION}/archlinux-bootstrap-${ARCHLINUX_VERSION}-x86_64.tar.gz.sig" -o archlinux-bootstrap-${ARCHLINUX_VERSION}-x86_64.tar.gz.sig \
   && pacman-key -v archlinux-bootstrap-${ARCHLINUX_VERSION}-x86_64.tar.gz.sig

RUN tar -zxf archlinux-bootstrap-${ARCHLINUX_VERSION}-x86_64.tar.gz
RUN sed -i 's/#Server = https:\/\/geo.mirror.pkgbuild.com.*/Server = https:\/\/geo.mirror.pkgbuild.com\/$repo\/os\/$arch/g' ./root.x86_64/etc/pacman.d/mirrorlist \
   && sed -i 's/#Server = https:\/\/mirrors.dotsrc.org.*/Server = https:\/\/mirrors.dotsrc.org\/archlinux\/$repo\/os\/$arch/g' ./root.x86_64/etc/pacman.d/mirrorlist && grep "^Server" ./root.x86_64/etc/pacman.d/mirrorlist

RUN cp ./config_files/do-system.sh ./root.x86_64/etc/profile.d/ \
   && cp ./config_files/wsl.conf ./root.x86_64/etc/ \
   && mkdir -p ./root.x86_64/root/install \
   && cp ./config_files/do-sys-install.sh ./root.x86_64/root/install/ \
   && chmod +x ./root.x86_64/root/install/do-sys-install.sh

RUN cd ./root.x86_64 \
   && tar -czf ../root.tar.gz * \
   && cd ../
